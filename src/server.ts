import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import http from 'http';
import WebSocket from 'ws';

const app = express();
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(cors());

const server = http.createServer(app);
const wss = new WebSocket.Server({server});

app.post('/auth', (req, res) => {
    res.set('content-type', 'application/json');
    if(req.body.username == 'username1' && req.body.password == 'password1')
        res.send('{"token":"ffaabb"}');
    else if(req.body.username == 'username2' && req.body.password == 'password2')
        res.send('{"token":"bbaaff"}');
    else {
        res.status(401);
        res.send({});
    }
});

class Command {
    type: string;
    data: string[];
    token: string;
}

wss.on('connection', (ws, req) =>{
    let username: string = null;
    if(req.url.includes('?token=ffaabb'))
        username = 'Emmanuel';
    else if(req.url.includes('?token=bbaaff'))
        username = 'Abigail';
    else
        ws.terminate();
    if(username != null) {
        wss.clients.forEach(ws => ws.readyState == ws.OPEN && ws.send(JSON.stringify({
            type: 'connected',
            data: [username]
        })));
    }
    ws.on('disconnect', (ws) => {
        wss.clients.forEach(ws2 => ws2.readyState == ws2.OPEN && ws2.send(JSON.stringify({
            type: 'disconnected',
            data: [username]
        })));
    });
    //TODO: Try/Catch to terminate connections on exception?
    ws.on('message', (msg: string) => {
        msg = msg.trim();
        let cmd: Command = Object.assign(new Command(), JSON.parse(msg));
        if(cmd.type == 'disconnected'){
            if(cmd.token == 'ffaabb')
                username = 'Emmanuel';
            else if(cmd.token == 'bbaaff')
                username = 'Abigail';
            else
                username = null;
            if(username != null) {
                wss.clients.forEach(ws => ws.readyState == ws.OPEN && ws.send(JSON.stringify({
                    type: 'disconnected',
                    data: [username]
                })));
            }
            ws.terminate();
        }
        else if(cmd.type == 'sendmessage') {
            if(cmd.token == 'ffaabb')
                username = 'Emmanuel';
            else if(cmd.token == 'bbaaff')
                username = 'Abigail';
            else
                username = null;
            if(username != null) {
                wss.clients.forEach(ws => ws.readyState == ws.OPEN && ws.send(JSON.stringify({
                    type: 'receivemessage',
                    data: [username, cmd.data[0]]
                })));
            }
            else {
                ws.terminate();
            }
        }
    });

});



server.listen(80, () => console.log('Chat server listening on port 80'));